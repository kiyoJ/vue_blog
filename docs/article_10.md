# 見積り作成練習その４

### 各種登録

**ニューの　登録（W）　ー＞単位の登録（T）を選択すると**  
<span class="scale"><img src="/img/mitu_lession3/t000.png" alt="" width="346" height="283" /></span>  

以下の画面が表示されます。

<span class="scale"> <img src="/img/mitu_lession3/t001.png" alt="" width="188" height="318" /></span>  
内訳の単位に使います。  
<!-- more -->  

他にセットする単位がある場合は追加ボタンをクリックして行番号と単位を入力してください。  
行番号　0　の単位が空白になっていますが、これは必要な物ですのでそのままにして置いて下さい。  
**メニューの　登録（W）　ー＞品名元ｺｰﾄﾞの登録（M）を選択**  
<span class="scale"><img src="/img/mitu_lession3/t002.png" alt="" width="284" height="387" /></span>    
見積内訳の品名コードです。
必要な品名がある場合はここで追加ボタンをクリックして品名コードと品名単位を入力して下さい。  
単位は単位の列をクリックすると先ほど登録した単位が表示されますので選択して下さい。  
ここで登録した品名コードが見積単価のコードで使用できます。  

**メニューの　登録（W）　ー＞品名元ﾃﾝﾌﾟﾚｰﾄの登録（W）を選択**  
<span class="scale"><img src="/img/mitu_lession3/t003.png" alt="" width="357" height="480" /></span>  
品名テンプレートはあらかじめ必要なコードと品名を登録して置き　仕様別の単価作成する場合に品名テンプレートボタンで設定名称から選択してコードと品名を一度挿入するようにする為の物です。  
**メニューの　登録（W）　ー＞施工箇所の登録（B）を選択**  
<span class="scale"><img src="/img/mitu_lession3/t004.png" alt="" width="332" height="420" /></span>  
施工箇所は沢山あると思いますのでほんとうに必要な物だけ登録するほうが良いと思います。  
**メニュー　登録（W）　ー＞仕様の登録（Y）**  
<span class="scale"><img src="/img/mitu_lession3/t005-1.png" alt="" width="556" height="422" /></span>     
仕様の登録は現在　主材「グラスウール保温筒」の見積仕様が表示されています。  
ロックウール保温筒等ほかの主材での見積仕様を作成する場合は主材の横にあるボックスをクリックして変更して下さい  
<span class="scale"><img src="/img/mitu_lession3/t005-2.png" alt="" width="556" height="422" /></span>  
黒枠の追加ボタンをクリックして赤枠の中から仕様に使う材料をひとつずつ順番に選択して水色枠の矢印ボタンをクリックして登録ボタンをクリックします。  
又は、黒枠の追加ボタンをクリック後直接ｺｰﾄﾞと見積仕様を入力しても構いません。  
**メニュー　編集（E）　ー＞見積番号の変更（Z）**  
<span class="scale"> <img src="/img/mitu_lession3/t006.png" alt="" width="242" height="159" /></span>  
 これは以前に見積した事のある物を仕様変更等で作成し直す場合に使用します。  
変更したい見積を開いて「見積番号の変更（Z）」をクリックします。  
<span class="scale"> <img src="/img/mitu_lession3/t006-2.png" alt="" width="338" height="159" />  </span>  
見積番号を確認してOKボタンをクリックします。  
<span class="scale"><img  src="/img/mitu_lession3/t006-3.png" alt="" width="400" /></span>  
見積番号と日付が新しく設定されその他の項目はコピーされていますのでこの見積を編集すると簡単に仕様変更ができます。  
