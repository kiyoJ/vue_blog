# はじめまして

<h2>ご挨拶</h2>

こんにちは、私どもの働いている会社は建設業の熱絶縁工事業と言う会社です。

聞きなれない業種ですが、一般には保温業と言っています。
それでも分かりにくい方が居られると思います。

一般家庭で主に行う工事は、水道管の破裂（凍結による）を防止や給湯管の温度を保つ事により灯油又はガスの使用料を控える事などがあります。

<hr />

大きなビル等では、機械室の空調機で作った温度を保ちながら各部屋に送る為に保温と言う工事を行います。

特に天井内での冷房は外気温度より低い為、結露と言う現象がおきます。

結露とはコップに氷みずを入れた時に表面につく水滴を思い浮かべると良いと思います。

この結露水がたまって天井に落ちてくると、シミになったりカビが発生したりします。

&nbsp;

<!-- <iframe src="https://maps.google.co.jp/maps?f=q&amp;hl=ja&amp;geocode=&amp;q=%E7%A6%8F%E5%B2%A1%E7%9C%8C%E7%B3%9F%E5%B1%8B%E9%83%A1%E9%A0%88%E6%81%B5%E7%94%BA%E5%A4%A7%E5%AD%97%E6%A4%8D%E6%9C%A8%EF%BC%91%EF%BC%96%EF%BC%98%E3%83%BC%EF%BC%91&amp;sll=33.600894,130.450287&amp;sspn=0.778941,0.929718&amp;ie=UTF8&amp;s=AARTsJq5h46bHXTx1NCAb9K_eYkQF-lQDA&amp;ll=33.610402,130.509596&amp;spn=0.025019,0.036478&amp;z=14&amp;iwloc=addr&amp;output=embed" width="500" height="400" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe> //-->

&nbsp;

<a style="text-align: left;" href="http://maps.google.co.jp/maps?f=q&amp;hl=ja&amp;geocode=&amp;q=%E7%A6%8F%E5%B2%A1%E7%9C%8C%E7%B3%9F%E5%B1%8B%E9%83%A1%E9%A0%88%E6%81%B5%E7%94%BA%E5%A4%A7%E5%AD%97%E6%A4%8D%E6%9C%A8%EF%BC%91%EF%BC%96%EF%BC%98%E3%83%BC%EF%BC%91&amp;sll=33.600894,130.450287&amp;sspn=0.778941,0.929718&amp;ie=UTF8&amp;ll=33.610402,130.509596&amp;spn=0.025019,0.036478&amp;z=14&amp;iwloc=addr&amp;source=embed">会社地図</a>

福岡高速を降りて飯塚方面へ粕屋警察から右へ（３５号線方面）曲がる

ルート６０７門松交差点を抜け２．２K（車で５分）位いくと会社の前に

須恵第二小学校入口の交差点があります。  
<strong>左側にファミリーマートがありますし花の看板の位置には塗装会社が建ちました。</strong>

<hr />


<strong> </strong>

<strong>会社の前の交差点（須恵第二小学校入口）</strong>

<img src="/img/kousaten.png" alt="kousaten" width="480" height="360" border="0" />

&nbsp;

<strong>交差点手前の左側入口（こちらに会社があります）</strong>

<img src="/img/kaisya_iriguti.png" alt="kaisya_iriguti" width="480" height="360" border="0" />

<strong>会社の玄関です</strong>

<img src="/img/kaisya_genkan.png" alt="kaisya_genkan" width="480" height="360" border="0" />

