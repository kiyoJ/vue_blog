# 保温見積書インストール

無料公開された保温工事専用見積書を<a href="/download_list/">ダウンロード</a>後Windows７へセットアップします。

windows7へのセットアップは注意が必要です。

問題なのはデータベースへ書き込む権限が必要だと言うことです。

ダウンロードしたykkanri_setup.zipを解凍します。右クリックして赤枠のすべて展開（T）をクリックします。

<img src="/img/setup/win7/7setup001.png" alt="" />

展開したファイルykkanri_setup.exeをダブルクリックします。

<img src="/img/setup/win7/7setup002.png" alt="" />

セットアップ画面が表示されました。<strong>次へ</strong>をクリックします。

<!-- more -->

<img src="/img/setup/win7/7setup004.png" alt="" />

ここでインストールするフォルダがc:￥Program Files￥yoshiki￥kanriになっていますので

このままではwindows7では使用できませんので注意しなければいけません。

<img src="/img/setup/win7/7setup005.png" alt="" />

参照ボタンで以下のようにc:￥Users￥Public￥kanriへ変更して下さい。

なおWindows7日本語版では「ユーザ￥パブリック」と表示されますが中身は「c:￥Users￥Public」です。

<img src="/img/setup/win7/7setup006.png" alt="" />

下の赤枠ようになっている事を確認して<strong>次へ</strong>をクリックします。

<img src="/img/setup/win7/7setup007.png" alt="" />

スタートメニューにショートカット作ると言っています。

問題ないので<strong>次へ</strong>をクリックします。

<img src="/img/setup/win7/7setup008.png" alt="" />

これもディスクトップへショートカットを作るかと聞いています。

問題なければ<strong>次へ</strong>をクリックします。

<img src="/img/setup/win7/7setup009.png" alt="" />

インストール（セットアップ）できる準備が整ったので<strong>インストール</strong>をクリックします。

<img src="/img/setup/win7/7setup010.png" alt="" />

インストールされているのでしばらく待ちます。

<img src="/img/setup/win7/7setup011.png" alt="" />

インストールの途中で以下の画面が表示されます。

データベースエンジンを以下のディレクトリ（フォルダ）に入れて良いかと聞いています。

このままで問題ないので、<strong>OK</strong>をクリックします。

<img src="/img/setup/win7/7setup012.png" alt="" />

インストールが完了しました。readme.txtにチェックがついています。

このまま<strong>完了</strong>をクリックして下さい。

<img src="/img/setup/win7/7setup015.png" alt="" />

お疲れ様でした。以下の注意書きが表示されますので読んで下さい。

<img src="/img/setup/win7/7setup016.png" alt="" />
