# 見積り作成練習その１


### セットアップ後見積を最初に起動してみる

左の赤枠　見積管理をクリックし右の赤枠　見積書をクリックします。

接続データを貴社ﾃﾞｰﾀからｻﾝﾌﾟﾙﾃﾞｰﾀへ変更するといくつか入力されたサンプルが見れます。

**画像が小さい場合は画像にマウスを乗せると拡大した画像がみれます。**  
<span class="scale">
<img  src="/img/mitu_lession1/mitu002.png" alt="" width="315" />
</span>  
何も設定されていない見積画面が表示されます。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu003.png" alt="" width="413" />
</span>  
<!-- more -->  
まずメニューの<strong>登録（W)</strong>ー＞<strong>貴社登録（J)</strong>をクリックして自分の会社情報を設定します。  
<span class="scale">
<img src="/img/mitu_lession1/mitu004.png" alt="" width="358" height="283" />
</span>  
&nbsp;

貴社設定画面

ロゴの入力枠がありますが、今回のバージョンでは意味が無いので設定はしません。

郵便番号を入力後「郵便番号/住所」ボタンをクリック  
<span class="scale">
<img  src="/img/mitu_lession1/mitu005.png" alt="" width="205" />
</span>  
クリックすると以下の画面が表示されますので検索ボタンをクリックー＞貼付けボタンをクリックします。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu006.png" alt="" width="315" />
</span>  
住所が挿入されていますが、番地は自力で入力します。

電話番号と、ファックス番号はTEL (xxx)xxx-xxx、FAX (xxx)-xxx-xxxxとTEL,FAXも入力して下さい。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu007.png" alt="" width="201" />
</span>  
&nbsp;

貴社の登録が済みましたのでここから簡単な見積を作成します。

メニューﾌｧｲﾙ（F)　ー＞　新規・追加(A)　をクリックします。  
<span class="scale">
<img src="/img/mitu_lession1/mitu008.png" alt="" width="150" height="195" />
</span>  
&nbsp;

見積番号と今日の日付が挿入されています。

次に得意先を入力します。

水色枠の右にある赤丸でかこまれた逆三角のボタンをクリックします。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu009.png" alt="" width="403" />
</span>  
以下の画面が表示されますので。追加ボタンをクリックしてコードと得意先名を入力します。

コードは得意先のイニシャル等を入力して下さい。

（イニシャル以外でも良いのですが、記録した見積を探すときにイニシャルで絞り込みをしますのでなるべくイニシャルで入力して下さい）

入力後OKボタンをクリックして下さい。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu010.png" alt="" width="157" />
</span>  
次に得意先担当者を入力します。

水色枠の右にある赤丸でかこまれた逆三角のボタンをクリックします。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu011.png" alt="" width="403" />
</span>  
以下の画面が表示されますので追加ボタンをクリックしてコードと氏名を入力して下さい。

今回コードは0000000001と入力しました。

1としないで0000000001としたのは表示される並びにルールがあり1 と 9 と 10　では上から10、1、 9の順序で並びます。

これは右端から一個づつ評価されるので10の右端が 0 なので9より小さいと判断されます。よって10、1、 9の並びになります。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu012.png" alt="" width="150" />
</span>  
&nbsp;

次に工事名称を入力します。

前回と同様に水色枠の右にある赤丸でかこまれた逆三角のボタンをクリックします。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu013.png" alt="" width="403" />
</span>  
以下の画面が表示されます。

追加ボタンをクリックしてコードと現場名を入力します。

その他の項目は、タックシール印刷に使用する情報ですので、タックシール印刷を使わない場合は必要ありません。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu014.png" alt="" width="363" />
</span>  
見積有効期限の逆三角のボタンをクリックします。

ここにはあらかじめいくつか入力されています。

別の有効期限がある場合は追加ボタンをクリックしてコードと有効期限を入力して下さい。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu015.png" alt="" width="157" />
</span>  
同様に取引方法にもあらかじめ入力されています。

別の取引方法がある場合は追加ボタンをクリックしてコードと取引方法を入力して下さい。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu016.png" alt="" width="156" />
</span>  
続いて作成者を入力します。

追加ボタンをクリックしてコードと氏名を入力して下さい。  
<span class="scale">
<img  src="/img/mitu_lession1/mitu017.png" alt="" width="151" height="142" />
</span>  
&nbsp;

長くなったので「その１」はここまでにします。  

この先は「その２」で説明します。
  