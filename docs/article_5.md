# 見積書特徴

<h4>[ 特徴 ]</h4>
<h4>仕様選択その１</h4><br >
図をクリックすると拡大します <br > 
<span class="scale">
<img  title="仕様選択その１" src="/img/WS000078.jpg"  width="300" />
</span>  
&nbsp;

<strong>仕様選択その２</strong>  
<span class="scale">
<img  title="仕様選択その２" src="/img/WS000082.jpg"  width="300" />
</span>
&nbsp;
<!-- more -->  
<strong>仕様にそった単価を複数一度に挿入できます</strong>  
<span class="scale">
<img  title="単価挿入" src="/img/WS000101.jpg"  width="300" />
</span>  
&nbsp;

<strong>挿入された単価の数量位置にて２＋(数値を入力後「＋、－、＊、／」)等 と入力すると簡易電卓で計算できます</strong>  
<span class="scale">
<img  title="簡易電卓" src="/img/WS000112.jpg"  width="300" />
</span>
&nbsp;

<strong>また数量及び位置でエンターキーを押すと即座に計算が実行され 後は印刷で終了。</strong>  
<span class="scale">
<img  title="印刷選択" src="/img/WS000219.jpg"  width="300" />
</span>  