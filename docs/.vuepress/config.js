module.exports = {
    base: '/',
    serviceWorker: true,
    title: '吉木保温（有）',
    head: [
        ['link', { rel: 'icon', type: 'image/png', href: '/img/favicon.png' }],
        ['link', { rel: "stylesheet", type: '', href: 'css/style.css' }],
        ['meta', { name: 'og:url', content: 'https://example.com/' }],
        ['meta', { name: 'og:type', content: 'website' }],
        ['meta', { name: 'og:title', content: 'example title' }],
        ['meta', { name: 'og:description', content: 'example description' }],
        ['meta', { name: 'og:image', content: '/og.png' }]
    ],

    themeConfig: {
        nav: [
            {
                text: 'ホーム', link: '/', exact: true
            },
            {
                text: 'ダウンロードリスト', link: '/download_list/', exact: false
            },
        ],
        sidebar: [
            '/',
            '/article_1',
            '/article_2',
            '/article_3',
            '/article_4',
            '/article_5',
            '/article_6',
            '/article_7',
            '/article_8',
            '/article_9',
            '/article_10',
            '/article_11',
            '/article_12',
        ],
    },
};
