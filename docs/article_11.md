# 見積り作成練習その５

<h2 class="tool-title">保存した見積書を開く</h2>
<span class="scale"><img src="/img/mitu_lession4/p0001.png" alt="" width="143" height="180" /></span>

メニュー　ﾌｧｲﾙ(F)　ー＞見積書を開く（O）をクリック

<span class="scale">
<img  src="/img/mitu_lession4/p0002.png" alt="" width="351" height="256" />
</span>
&nbsp;

得意先　IDにコードの頭文字を入力して右ボックス枠から選択します。表から目的の行をクリックしてOKボタンをクリックすると保存した見積書が開きます。
