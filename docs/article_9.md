# 見積り作成練習その３

#### 見積を作成すると客先から「日程？」「ネット？」とか、よく尋ねられます。

自分の場合電話でネットを言う事もしばしば、、、しかし自分が言ったネット金額を忘れる事もあります。

２行位のメモなら見積に記入する事が可能です。

**メニュー　ﾌｧｲﾙ（F）　ー＞備考入力（J）を選びます。**  
<span class="scale">
<img src="/img/mitu_lession2/bikou_run.png" alt="" width="146" height="226" />
</span>  
&nbsp;

**備考画面に入力中**  
<span class="scale">
<img src="/img/mitu_lession2/bikou.png" alt="" width="343" />
</span>  
<!-- more -->  

**備考が印刷されます。**  
<span class="scale">
<img src="/img/mitu_lession2/bikou-print.png" alt="" width="415" />
</span>  
&nbsp;

最近消費税も引き上げられそうな感じですが、その場合は消費税率を変更して下さい。  
私の客先では未だに税込で金額を決定されるところもあります。  
見積に対して６０％の掛率で税込と言われる事もしばしば、、、  
本来消費税は国への預り税なのですが、、、しかしそんな事を言うもんなら他に業者はいくらでもいるよ。  
と言われます。そこで少しでも怪我が少なくなるように、消費税要覧で「内消費税？？？？を含みます。」  
に設定すると税込の見積になります。（解決策ではないのですが）  
<span class="scale">
<img  src="/img/mitu_lession2/syouhizei.png" alt="" width="417" />
</span>  

**以下消費税を含んだ見積書です。**    
<span class="scale">
<img  src="/img/mitu_lession2/syouhizei-print.png" alt="" width="417" />
</span>  
&nbsp;
